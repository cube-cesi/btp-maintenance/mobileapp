import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import Login from '../screens/auth/Login'
import Home from '../components/Home';
import HomeScreen from '../screens/home/Home';
import CardRoom from '../screens/rooms/CardRoom';
import RoomLocalisation from '../components/RoomLocalisation'
import QRCodeScanner from '../components/QRCodeScanner';
import InformationsRoom from '../screens/room/InformationsRoom'

const AuthStack = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions:{
            header: () => null
        }
    }
})

const AppStack = createStackNavigator({
    Home: { 
        screen: HomeScreen,
        navigationOptions: {
           header: () => null
        }
    },
    InformationsRoom: {
        screen: InformationsRoom,
        navigationOptions: {
            header: () => null
        }
    },
    QRCodeScanner: {
        screen: QRCodeScanner,
        navigationOptions: {
            title: 'QR code'
        }
    },
    RoomLocalisation: {
        screen: RoomLocalisation,
        navigationOptions: {
            title: 'Géolocalisation'
        }
    }
})

// Un SwitchNavigator permet de ne pas pouvoir revenir sur les vues précedentes
const RootNavigator = createSwitchNavigator({
    Auth: AuthStack,
    App: AppStack
})

export default createAppContainer(RootNavigator)