import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";
import EvilIconsIcon from "react-native-vector-icons/EvilIcons";

function SignUp(props) {
  return (
    <View style={styles.root}>
      <StatusBar
        animated
        barStyle="light-content"
        backgroundColor="rgba(0,0,0,0)"
      />
      <View style={styles.background}>
      <View style={styles.progressBarColumn}>
            <Text style={styles.text3}>Inscription</Text>
            <View style={styles.form}>
              <View style={styles.lastname}>
                <EvilIconsIcon name="user" style={styles.icon9}></EvilIconsIcon>
                <TextInput
                  placeholder="Prénom"
                  placeholderTextColor="rgba(255,255,255,1)"
                  secureTextEntry={false}
                  style={styles.firstnameInput}
                ></TextInput>
              </View>
              <View style={styles.name}>
                <EvilIconsIcon name="user" style={styles.icon5}></EvilIconsIcon>
                <TextInput
                  placeholder="Nom de famille"
                  placeholderTextColor="rgba(255,255,255,1)"
                  secureTextEntry={false}
                  style={styles.lastnameInput}
                ></TextInput>
              </View>
              <View style={styles.email}>
                <EvilIconsIcon
                  name="envelope"
                  style={styles.icon6}
                ></EvilIconsIcon>
                <TextInput
                  placeholder="Adresse e-mail"
                  placeholderTextColor="rgba(255,255,255,1)"
                  secureTextEntry={false}
                  style={styles.emailInput}
                ></TextInput>
              </View>
              <View style={styles.password}>
                <EvilIconsIcon name="lock" style={styles.icon7}></EvilIconsIcon>
                <TextInput
                  placeholder="Mot de passe"
                  placeholderTextColor="rgba(255,255,255,1)"
                  secureTextEntry={true}
                  style={styles.passwordInput}
                ></TextInput>
              </View>
            </View>
          </View>
          <View style={styles.progressBarColumnFiller}></View>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Timeline")}
            style={styles.button}
          >
            <Text style={styles.sinscrire}>S&#39;inscrire</Text>
          </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  background: {
    flex: 1
  },
  rect2: {
    flex: 1
  },
  rect2_imageStyle: {},
  progressBar: {
    height: 40,
    flexDirection: "row",
    marginLeft: 31,
    marginRight: 31
  },
  icon3: {
    color: "rgba(255,200,83,1)",
    fontSize: 35,
    width: 40,
    height: 36,
    marginLeft: 45,
    marginTop: 2
  },
  rect5: {
    width: 50,
    height: 7,
    backgroundColor: "rgba(230, 230, 230,1)",
    opacity: 0.75,
    borderRadius: 40,
    marginTop: 16
  },
  icon3Filler: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center"
  },
  icon4: {
    color: "rgba(255,255,255,1)",
    fontSize: 40,
    width: 34,
    height: 40,
    opacity: 0.75,
    marginRight: 44
  },
  text3: {
    color: "#1a202c",
    fontSize: 24,
    marginTop: 62,
    alignSelf: "center",
    fontWeight: "bold"
  },
  form: {
    height: 271,
    justifyContent: "space-between",
    marginTop: 30
  },
  lastname: {
    height: 59,
    backgroundColor: "rgba(160,174,192,1)",
    borderRadius: 5,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  icon9: {
    color: "rgba(255,255,255,1)",
    fontSize: 33,
    width: 33,
    height: 33,
    marginLeft: 16,
    alignSelf: "center"
  },
  firstnameInput: {
    height: 30,
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    flex: 1,
    marginRight: 17,
    marginLeft: 12,
    marginTop: 14
  },
  name: {
    height: 59,
    backgroundColor: "rgba(160,174,192,1)",
    borderRadius: 5,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  icon5: {
    color: "rgba(255,255,255,1)",
    fontSize: 33,
    width: 33,
    height: 33,
    marginLeft: 16,
    alignSelf: "center"
  },
  lastnameInput: {
    height: 30,
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    flex: 1,
    marginRight: 17,
    marginLeft: 12,
    marginTop: 14
  },
  email: {
    height: 59,
    backgroundColor: "rgba(160,174,192,1)",
    borderRadius: 5,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  icon6: {
    color: "rgba(255,255,255,1)",
    fontSize: 33,
    marginLeft: 15,
    alignSelf: "center"
  },
  emailInput: {
    height: 30,
    color: "rgba(255,255,255,1)",
    flex: 1,
    marginRight: 17,
    marginLeft: 13,
    marginTop: 14
  },
  password: {
    height: 59,
    backgroundColor: "rgba(160,174,192,1)",
    borderRadius: 5,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  icon7: {
    color: "rgba(255,255,255,1)",
    fontSize: 33,
    marginLeft: 15,
    marginTop: 13
  },
  passwordInput: {
    height: 30,
    color: "rgba(255,255,255,1)",
    flex: 1,
    marginRight: 17,
    marginLeft: 13,
    marginTop: 14
  },
  progressBarColumn: {
    marginTop: 58,
    marginLeft: 41,
    marginRight: 41
  },
  progressBarColumnFiller: {
    flex: 1
  },
  button: {
    height: 55,
    backgroundColor: "rgba(255,200,83,1)",
    borderRadius: 5,
    justifyContent: "center",
    marginBottom: 300,
    marginLeft: 41,
    marginRight: 41
  },
  sinscrire: {
    width: 66,
    color: "rgba(255,255,255,1)",
    alignSelf: "center"
  }
});

export default SignUp;