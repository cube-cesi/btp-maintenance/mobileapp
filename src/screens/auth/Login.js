import React, { Component } from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity, TouchableHighlight, Alert, Modal } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as Location from 'expo-location';
import { Picker } from '@react-native-picker/picker';
import axios from 'axios'
import { URL_BACK_END } from '../../environment/Environment'

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            modalVisible: false,
            rooms: [],
            selectedRoom: null
        }
    }

    createSession = (member) => {
        axios.post(`${URL_BACK_END}/api/auth/login`, { member }, { withCredentials: true })
            .then(response => {
                axios.patch(`${URL_BACK_END}/api/members/last-connection/${response.data.member.id}`, { lastConnection: new Date().toLocaleString() })
                this.geolocalisationSalle()
            })
    }

    connexionUtilisateur = () => {

        const { email, password } = this.state;


        if (email == "" || password == "")
            return

        let member = {
            email: email,
            password: password
        }
        console.log(`${URL_BACK_END}/api/auth/checkLogin`);

        axios.post(`${URL_BACK_END}/api/auth/checkLogin`, { member }, { withCredentials: true })
            .then(response => {

                if (response.data.logged_in) {
                    //Si l'utilisateur à activé la 2FA
                    if (response.data.member.activate2FA) {
                        // Gestion du code 2FA ici !
                        // En attendant on se connecte sans vérif
                        this.createSession(member)
                    } else {
                        this.createSession(member)
                    }
                }
            })
            .catch(error => Alert.alert(error.response.errors))
    }

    connexionAnon = () => {
        axios.get(`${URL_BACK_END}/api/auth/anonymous`)
            .then(response => {
                // Ajouter la gestion anonyme côté client ici !
                this.geolocalisationSalle()
            })
    }

    AlertGeolocalisationRoom(room) {
        Alert.alert(
            "Localisation de la salle",
            `Vous êtes proche de ${room.attributes.name}. Voulez-vous acceder à sa page ?`,
            [
                {
                    text: "Non",
                    onPress: () => this.props.navigation.navigate("Home"),
                    style: "cancel"
                },
                {
                    text: "Oui",
                    onPress: () => this.props.navigation.navigate("InformationsRoom", { name_url: room.attributes.name_url })
                }
            ],
            { cancelable: false }
        );
    }

    async geolocalisationSalle() {
        let { status } = await Location.requestPermissionsAsync();
        if (status !== 'granted') {
            Alert.alert('Erreur', "Permission de géolocalisation non accordé.\nRedirection vers la page d'acceuil.")
            this.props.navigation.navigate("Home")
        }
        let location = await Location.getCurrentPositionAsync({ accuracy: Location.LocationAccuracy.Highest });
        let x = Math.ceil(location.coords.latitude)
        let y = Math.ceil(location.coords.longitude)
        let z = Math.ceil(location.coords.altitude)
        this.getRoomsByCoords(`${x},${y},${z}`)
    }

    getRoomsByCoords(coords) {
        axios.get(`${URL_BACK_END}/api/rooms/localisation/${coords}`)
            .then(response => {
                if (response.data.data.length == 0) {
                    this.props.navigation.navigate("Home")
                }
                else if (response.data.data.length == 1) {
                    this.AlertGeolocalisationRoom(response.data.data[0])
                }
                else {
                    this.setState({ rooms: response.data.data, selectedRoom: response.data.data[0].attributes.name_url, modalVisible: true })
                }
            })
    }

    render() {
        const { email, password, rooms, selectedRoom, modalVisible } = this.state;

        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        console.log("close")
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>Il y a plusieurs salles correspondant à votre position,
                            veuillez choisir celle que vous voulez voir ou annuler</Text>
                            <View style={styles.picker}>
                                <Picker
                                    selectedValue={selectedRoom}
                                    style={{ height: 50, width: 150 }}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ selectedRoom: itemValue })}
                                >
                                    {
                                        rooms.map(room =>
                                            <Picker.Item key={room.id} label={room.attributes.name} value={room.attributes.name_url} />
                                        )
                                    }
                                </Picker>
                            </View>

                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#ffc853" }}
                                onPress={() => {
                                    this.props.navigation.navigate("InformationsRoom", { name_url: selectedRoom })
                                }}
                            >
                                <Text style={styles.textStyle}>Valider</Text>
                            </TouchableHighlight>

                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#ffc853" }}
                                onPress={() => {
                                    this.props.navigation.navigate('Home')
                                }}
                            >
                                <Text style={styles.textStyle}>Annuler</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>

                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: "https://yt3.ggpht.com/ytc/AAUvwniJcudFBvjhncQ4O0DaTopCR9eFqPV6hoGGZsVl4A=s900-c-k-c0x00ffffff-no-rj"
                    }}
                />
                <Text style={styles.header}>Se connecter</Text>
                <View style={styles.inputView} >
                    <TextInput
                        style={styles.inputText}
                        placeholder="Adresse mail"
                        placeholderTextColor="#003f5c"
                        onChangeText={text => this.setState({ email: text })} />
                </View>
                <View style={styles.inputView} >
                    <TextInput
                        secureTextEntry
                        style={styles.inputText}
                        placeholder="Mot de passe"
                        placeholderTextColor="#003f5c"
                        onChangeText={text => this.setState({ password: text })} />
                </View>
                <TouchableOpacity style={styles.loginBtn} onPress={this.connexionUtilisateur}>
                    <Text style={styles.loginText}>Valider</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.anoBtn} onPress={this.connexionAnon}>
                    <Text style={styles.anoText}>Connexion anonyme</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.registerBtn}>
                    <Text style={styles.registerText}>S'inscrire</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    tinyLogo: {
        width: 50,
        height: 50,
    },
    header: {
        fontWeight: "bold",
        fontSize: 42,
        color: "#1a202c",
        marginBottom: 40
    },
    inputView: {
        width: "80%",
        backgroundColor: "#f7fafc",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "#003f5c"
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#ffc853",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    loginText: {
        color: "white"
    },
    anoBtn: {
        width: "80%",
        backgroundColor: "#2d3748",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    anoText: {
        color: "#f7fafc"
    },
    registerBtn: {
        width: "80%",
        backgroundColor: "#2d3748",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10
    },
    registerText: {
        color: "#f7fafc"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        width: wp('80%'),
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        marginBottom: 10,
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width: wp('30%')
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "bold"
    },
    picker: {
        height: hp('10%'),
        borderRadius: 25,
        justifyContent: "center",
        padding: 20
    }
});

export default Login;