import React, { Component } from 'react';
import { TouchableOpacity } from "react-native";
import styled from 'styled-components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { withNavigation } from 'react-navigation';

class CardRoom extends Component {
    constructor(props){
		super(props);
    }

	handleSubmit = () => {
		let name_url = this.props.name_url;
		this.props.navigation.navigate("InformationsRoom", { name_url: name_url })
	}

    render(){
        return(
			<TouchableOpacity
                            onPress={() => this.handleSubmit(this.props.issues, this.props.name)}
                        >
            	<Container>
					<Cover>
						<Image source={{uri: "https://img.icons8.com/ios/452/room.png"}}/>
					</Cover>
					<Content>
						<Title>{this.props.name}</Title>
						<PriceCaption>{this.props.issues} tickets</PriceCaption>
					</Content>
            	</Container>
			</TouchableOpacity>
        )
    }
}

const Container = styled.View`
	background: #fff;
	height: ${hp('23%')}px;
	width: ${wp('40%')}%;
	border-radius: 14px;
	margin: 18px;
	margin-top: 10px;
	box-shadow: 0 5px 15px rgba(0, 0, 0, 0.15);
`;

const Cover = styled.View`
	width: 100%;
	height: ${hp('15%')}px;
	border-top-left-radius: 14px;
	border-top-right-radius: 14px;
	overflow: hidden;
`;

const Image = styled.Image`
	width: 100%;
	height: 100%;
`;

const Content = styled.View`
	padding-top: 10px;
	flex-direction: column;
	align-items: center;
	height: 60px;
`;

const Title = styled.Text`
	color: #3c4560;
	font-size: 20px;
	font-weight: 600;
`;

const PriceCaption = styled.Text`
	color: #b8b3c3;
	font-size: 15px;
	font-weight: 600;
	margin-top: 4px;
`;

export default withNavigation(CardRoom);