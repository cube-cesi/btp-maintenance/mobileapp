import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { withNavigation } from 'react-navigation';
import ShowRooms from "./ShowRooms";
import Divider from "../../components/utils/misc/Divider";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class Floors extends Component {
  constructor(props) {
    super(props);

    this.state = {
      floors: [],
      idFloorSelected: 0
    }
  }

  componentDidMount() {
    this.getFloors();
  }

  getFloors() {
    axios.get(`${URL_BACK_END}/api/floors`)
      .then(res => {
        this.setState({ floors: res.data, idFloorSelected: res.data[0].id });
      })
      .catch(error => console.log(error))
  }

  handleClickTab = (tabId) => {
    this.setState({ idFloorSelected: tabId });
  }

  render() {
    const { floors, idFloorSelected } = this.state;

    return (
      <View>
        <View style={styles.headerTabs}>
          {
            floors.map(floor =>
              <TouchableOpacity
                key={floor.id}
                style={
                  floor.id === idFloorSelected ?
                    (styles.selectedTab) :
                    (styles.buttonTab)
                }
                onPress={
                  () => this.handleClickTab(floor.id)
                }
              >
                <Text style={styles.textTab}>{floor.name}</Text>
              </TouchableOpacity>
            )
          }
        </View>
        <Divider style={styles.divider}></Divider>
        <ShowRooms {...this.props} navigation={this.props.navigation} idFloor={idFloorSelected}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  headerTabs: {
    height: hp('10%'),
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  buttonTab: {
    width: wp('35%'),
    alignSelf: "stretch",
    justifyContent: "center"
  },
  textTab: {
    color: "rgba(26,32,44,1)",
    fontSize: 14,
    alignSelf: "center"
  },
  selectedTab: {
    width: wp('35%'),
    alignSelf: "stretch",
    justifyContent: "center",
    borderColor: "rgba(255,200,83,1)",
    borderWidth: 0,
    borderBottomWidth: 3
  },
  divider: {
    width: wp('97%'),
    height: 1,
    marginTop: 11,
    marginLeft: 7
  }
});

export default withNavigation(Floors);