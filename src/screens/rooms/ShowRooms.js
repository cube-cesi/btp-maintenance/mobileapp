import React, { Component } from 'react';
import styled from 'styled-components';
import { ScrollView, StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { withNavigation } from 'react-navigation';
import CardRoom from './CardRoom';
import Icon from "react-native-vector-icons/Ionicons";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class ShowRooms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rooms: [],
            loaded: true
        }
    }

    componentDidMount() {
        this.getRoomsOfFloor();
    }

    componentDidUpdate (prevProps, prevState) {
        if (prevProps != this.props)
            this.getRoomsOfFloor()
    }

    getRoomsOfFloor = () => {  
        axios.get(`${URL_BACK_END}/api/rooms/mobile/where/${this.props.idFloor}`)
        .then(res => {
            this.setState({ rooms: res.data });
        })
        .catch(error => console.log(error))
    }

    renderRoomsByPage = () => {
        const { rooms } = this.state;
        const arroundRooms = Math.round(rooms.length / 2);
        let renderView = [];

        for (let i = 0; i < arroundRooms; i++) {
            let children = [];
            let checkerId = i > 0 ? (i * 2) : (i);

            if (rooms[checkerId] !== undefined) {
                children.push(<ColumnOne key={"column1" + i}>
                    <CardRoom {...this.props} navigation={this.props.navigation} name_url={rooms[checkerId].name_url} name={rooms[checkerId].name} issues={rooms[checkerId].issues} />
                </ColumnOne>);
            }

            if (rooms[checkerId + 1] !== undefined) {
                children.push(<ColumnTwo key={"column2" + checkerId}>
                        <CardRoom {...this.props} navigation={this.props.navigation} name_url= {rooms[checkerId + 1].name_url} name={rooms[checkerId + 1].name} issues={rooms[checkerId + 1].issues} />
                </ColumnTwo>);
            }

            renderView.push(<ItemsLayout key={"layout" + i}>{children}</ItemsLayout>);
        }

        return renderView;
    }

    render() {
        const { loaded } = this.state;
        return (
            <Container>
                {
                    loaded &&
                    <ScrollView horizontal={false} style={styles.containerScroll}>
                        {
                            this.renderRoomsByPage()
                        }
                    </ScrollView>
                }
            </Container>
        )
    }
}

const Container = styled.View`
`;

const ItemsLayout = styled.View`
    flex-direction: row;
    flex: 1;
    margin-left: 5%;
`;

const ColumnOne = styled.View`
margin-right: ${wp('5%')}%;`;

const ColumnTwo = styled.View``;

const styles = StyleSheet.create({
    containerScroll: {
        width: wp('100%'),
        height: hp('72%')
    }
});

export default withNavigation(ShowRooms);