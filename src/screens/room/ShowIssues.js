import React, { Component } from 'react';
import styled from 'styled-components';
import { ScrollView, StyleSheet, View, TouchableOpacity, Text } from "react-native";
import CardIssue from './CardIssue';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class ShowMaterials extends Component {
    constructor(props) {
        super(props);
        this.state = {
            issues: [],
            loaded: false
        }
    }

    componentDidMount() {
        this.getIssuesByRoom();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps != this.props)
            this.getIssuesByRoom();
    }

    getIssuesByRoom() {
        axios.get(`${URL_BACK_END}/api/issues/room/${this.props.name_url}`)
        .then(res => {
            this.setState({ issues: res.data, loaded: true });
        })
        .catch(error => console.log(error));
    }

    renderIssues = () => {
        const { issues } = this.state;
        let render = [];

        issues.map(issue => {
            render.push(
                <ItemsLayout key={issue.id}>
                    <CardIssue id={issue.id} name={issue.title} date={issue.created_at} desc={issue.description}/>
                </ItemsLayout>
            );
        })

        return render;
    }

    render() {
        const { loaded } = this.state;
        return (
            <Container>
                {
                    loaded &&
                    <ScrollView horizontal={false} style={styles.containerScroll}>
                        {
                            this.renderIssues()
                        }
                    </ScrollView>
                }
            </Container>
        )
    }
}

const Container = styled.View`
`;

const ItemsLayout = styled.View`
    flex-direction: row;
    flex: 1;
    margin-left: 5%;
`;

const styles = StyleSheet.create({
    containerScroll: {
        width: wp('100%'),
        height: hp('56.5%')
    }
});

export default ShowMaterials;