import React, { Component } from 'react';
import styled from 'styled-components';
import { StyleSheet, TouchableHighlight, TouchableOpacity, Text, View, Modal, ScrollView, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import normalize from 'react-native-normalize';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class CardIssue extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            visible: false,
            issue: null,
            comments: [],
            text: ''
        }
    }

    componentDidMount() {
        this.getComments();
    }

    getComments = () => {
        axios.get(`${URL_BACK_END}/api/issues/${this.props.id}`)
        .then(res => {
            this.setState({ comments: res.data.comments, issue: res.data.data, loaded: true });
        })
        .catch(error => console.log(error));
    }

    submitComment() {
        let comment = {
            issue_id: this.props.id,
            description: this.state.text
        }
        
        axios.post(`${URL_BACK_END}/api/comments/mobile`, comment)
        .then(res => {
            this.closeModal()
        })
        .catch(error => console.log(error));
    }

    showModal = () => {
        this.setState({ visible: true });
    }

    closeModal = () => {
        this.setState({ visible: false });
    }

    render() {
        const { visible, comments, issue, loaded } = this.state;
        if (!loaded)
            return (<View></View>);

        let creator = 'Anonyme'
        if (issue.memberFirstname != null && issue.memberLastname != null)
            creator = `${issue.memberFirstname} ${issue.memberLastname}`

        return (

            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={visible}
                    onRequestClose={() => {
                        console.log("close")
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>#{issue.id} - {issue.priority.name} - {creator} </Text>
                            <Text style={styles.modalTitle}>Titre du ticket</Text>
                            <Text>{this.props.desc}</Text>
                            <Text style={styles.commentsText}>Commentaires:</Text>
                            <ScrollView horizontal={false} style={styles.containerScroll}>
                            {
                                comments.map(comment => {
                                    <View style={styles.viewComments}>
                                        <Text style={styles.modalAuthorComment}>{comment.memberFirstanme} {comment.memberLastname} - {comment.created_at}</Text>
                                        <Quantity>{comment.description}</Quantity>
                                    </View>
                                })
                            }
                            </ScrollView>
                            <View style={styles.inputView} >
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Votre commentaire"
                                    editable={true} 
                                    onChangeText={(value) => this.setState( {text: value} )}  />
                            </View>
                            <View style={styles.buttonView}>
                                <TouchableHighlight
                                    style={{ ...styles.openButton, backgroundColor: "#B2371D" }}
                                    onPress={() => {
                                        this.submitComment()
                                    }}
                                >
                                    <Text style={styles.textStyle}>Poster</Text>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    style={{ ...styles.openButton, backgroundColor: "#ffc853" }}
                                    onPress={() => {
                                        this.closeModal()
                                    }}
                                >
                                    <Text style={styles.textStyle}>Fermer</Text>
                                </TouchableHighlight>
                            </View>

                        </View>
                    </View>
                </Modal>
                <Content>
                    <Title>{this.props.name}</Title>
                    <DateCreated>Mise en ligne: {this.props.date}</DateCreated>
                    <Quantity>{this.props.desc}</Quantity>
                    <TouchableOpacity onPress={() => this.showModal()} style={styles.button}>
                        <Text style={styles.caption}>Voir le ticket</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        )
    }
}

const Content = styled.View`
	padding-top: 10px;
	flex-direction: column;
	align-items: center;
	height: ${normalize(60, 'height')}px;
`;

const Title = styled.Text`
	color: #3c4560;
	font-size: 20px;
	font-weight: 600;
`;

const DateCreated = styled.Text`
	color: #b8b3c3;
	font-size: 15px;
	font-weight: 600;
	margin-top: 4px;
`;

const Quantity = styled.Text`
	color: #b8b3c3;
	font-size: 15px;
	font-weight: 600;
	margin-top: 6px;
`;

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#ffc853",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 5,
        paddingLeft: 16,
        paddingRight: 16,
        marginTop: 10,
        height: 44,
        width: 150
    },
    caption: {
        color: "#fff",
        fontSize: 17
    },
    container: {
        backgroundColor: "#fff",
        height: normalize('150'),
        width: normalize('325'),
        borderRadius: 14,
        margin: 10,
        marginTop: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        width: wp('80%'),
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,

    },
    modalText: {
        marginBottom: 15,
        fontSize: 18,
        textAlign: "center",
        fontWeight: "bold"
    },
    modalTitle: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "bold",
    },
    commentsText: {
        marginTop: 10,
        marginBottom: 5,
        textDecorationLine: "underline"
    },
    modalAuthorComment: {
        width: wp('50%'),
        textAlign: "left",
        fontWeight: "bold",
    },
    viewComments: {
        marginBottom: 5,
        borderColor: "rgba(128,128,128,1)",
        borderWidth: 0,
        borderBottomWidth: 1
    },
    inputView: {
        width: "100%",
        backgroundColor: "#f7fafc",
        borderRadius: 25,
        height: hp('5%'),
        marginTop: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "#003f5c"
    },
    containerScroll: {
        width: wp('60%'),
        height: hp('15%'),
    },
    buttonView: {
        alignItems: "center",
    },
    openButton: {
        backgroundColor: "#F194FF",
        textAlign: "center",
        marginTop: 10,
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width: wp('30%')
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
})

export default CardIssue;