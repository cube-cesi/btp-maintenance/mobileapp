import React, { Component } from "react";
import { StyleSheet, View, StatusBar, Text, ScrollView } from "react-native";
import TabCategory from './TabCategory';
import Footer from "../../components/include/Footer";

function InformationsRoom(props) {
    return (
        <View style={styles.container}>
            <StatusBar show />
            <View style={styles.headerTabs1Column}>
                <TabCategory {...props} url={props.navigation.state.params.name_url}/>
            </View>
            <View style={styles.headerTabs1ColumnFiller}></View>
            <Footer navigation={props.navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    headerTabs1Column: {
        marginTop: 25,
        marginLeft: -2,
        marginRight: 2
    },
    headerTabs1ColumnFiller: {
        flex: 1
    }
});

export default InformationsRoom;
