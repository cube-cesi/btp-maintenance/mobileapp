import React, { Component } from 'react';
import styled from 'styled-components';
import { StyleSheet, TouchableOpacity, Text, View, Modal, TouchableHighlight, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RNPickerSelect from "react-native-picker-select";
import { Picker } from '@react-native-picker/picker';
import normalize from 'react-native-normalize';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class CardMaterial extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            priorities: [],
            selectedPriority: 0,
            title: '',
            description: ''
        }
    }

    componentDidMount() {
        this.getPriorities();
    }

    getPriorities() {
        axios.get(`${URL_BACK_END}/api/priorities`)
        .then(res => {
            this.setState({ priorities: res.data.data, selectedPriority: res.data.data[0].id });
        })
        .catch(error => console.log(error))
    }

    showModal = () => {
        this.setState({ visible: true });
    }

    closeModal = () => {
        this.setState({ visible: false });
    }

    submitIssue = () => {
        let issue = {
            title: this.state.title,
            description: this.state.description,
            material_id: this.props.id,
            priority_id: this.state.selectedPriority,
            anonyme: false
        }
        axios.post(`${URL_BACK_END}/api/issues/mobile`, issue)
        .then(res => {
            this.closeModal();
        })
        .catch(error => console.log(error))
    }

    render() {

        const { visible, priorities, selectedPriority, title, description } = this.state;

        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={visible}
                    onRequestClose={() => {
                        console.log("close")
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>Nouveau ticket</Text>
                            <View style={styles.inputView} >
                                <TextInput
                                    style={styles.inputText}
                                    value={this.props.name}
                                    editable={false} />
                            </View>

                            <View style={styles.picker}>
                                {<RNPickerSelect
                                    onValueChange={(value) => console.log(value)}
                                    items={[
                                        { label: "Urgent", value: "2" },
                                        { label: "Faible", value: "1" },
                                        { label: "Moyen", value: "0" },
                                    ]}
                                />}
                                
                            </View>

                            <View style={styles.inputView} >
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Titre du ticket"
                                    editable={true} 
                                    onChangeText={(value) => this.setState({ title: value })}/>
                            </View>

                            <View style={styles.inputAreaView} >
                                <TextInput
                                    style={styles.inputText}
                                    placeholder="Description du ticket"
                                    multiline={true}
                                    editable={true}
                                    numberOfLines={10}
                                    onChangeText={(value) => this.setState({ description: value })}
                                />
                            </View>

                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#ffc853" }}
                                onPress={() => {
                                    this.submitIssue()
                                }}
                            >
                                <Text style={styles.textStyle}>Valider</Text>
                            </TouchableHighlight>

                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#ffc853" }}
                                onPress={() => {
                                    this.closeModal()
                                }}
                            >
                                <Text style={styles.textStyle}>Fermer</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
                <Content>
                    <Title>{this.props.name}</Title>
                    <DateCreated>Mise en ligne: {this.props.date}</DateCreated>
                    <Quantity>Quantités: {this.props.quantity}</Quantity>
                    <TouchableOpacity onPress={() => this.showModal()} style={styles.button}>
                        <Text style={styles.caption}>Nouveau ticket</Text>
                    </TouchableOpacity>
                </Content>
            </View>
        )
    }
}

const Content = styled.View`
	padding-top: 10px;
	flex-direction: column;
	align-items: center;
	height: ${normalize(60, 'height')}px;
`;

const Title = styled.Text`
	color: #3c4560;
	font-size: 20px;
	font-weight: 600;
`;

const DateCreated = styled.Text`
	color: #b8b3c3;
	font-size: 15px;
	font-weight: 600;
	margin-top: 4px;
`;

const Quantity = styled.Text`
	color: #b8b3c3;
	font-size: 15px;
	font-weight: 600;
	margin-top: 6px;
`;

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#ffc853",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 5,
        paddingLeft: 16,
        paddingRight: 16,
        marginTop: 10,
        height: 44,
        width: 150
    },
    caption: {
        color: "#fff",
        fontSize: 17
    },
    container: {
        backgroundColor: "#fff",
        height: normalize('150'),
        width: normalize('325'),
        borderRadius: 14,
        margin: 10,
        marginTop: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        width: wp('80%'),
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        marginBottom: 10,
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width: wp('30%')
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "bold"
    },
    inputView: {
        width: "80%",
        backgroundColor: "#f7fafc",
        borderRadius: 25,
        height: hp('5%'),
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputAreaView: {
        width: "80%",
        backgroundColor: "#f7fafc",
        borderRadius: 25,
        height: hp('15%'),
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    picker: {
        height: hp('10%'),
        borderRadius: 25,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "#003f5c"
    },
})

export default CardMaterial;