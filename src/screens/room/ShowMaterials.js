import React, { Component } from 'react';
import styled from 'styled-components';
import { ScrollView, StyleSheet, View, TouchableOpacity, Text } from "react-native";
import CardMaterial from './CardMaterial';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import normalize from 'react-native-normalize';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class ShowMaterials extends Component {
    constructor(props) {
        super(props);
        this.state = {
            materials: [],
            loaded: false
        }
    }

    componentDidMount() {
        this.getMaterialsByRoom();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps != this.props)
            this.getMaterialsByRoom();
    }

    getMaterialsByRoom() {
        axios.get(`${URL_BACK_END}/api/materials/room/${this.props.name_url}`)
        .then(res => {
            this.setState({ materials: res.data, loaded: true });
        })
        .catch(error => console.log(error))
    }

    renderMaterials = () => {
        const { materials } = this.state;
        let render = [];

        materials.map(material => {
            render.push(
                <ItemsLayout key={material.id}>
                    <CardMaterial id={material.id} name={material.name} date={material.created_at} quantity={material.quantity}/>
                </ItemsLayout>
            );
        })

        return render;
    }

    render() {
        const { loaded } = this.state;
        return (
            <Container>
                {
                    loaded &&
                    <ScrollView horizontal={false} style={styles.containerScroll}>
                        {
                            this.renderMaterials()
                        }
                    </ScrollView>
                }
            </Container>
        )
    }
}

const Container = styled.View`
`;

const ItemsLayout = styled.View`
    flex-direction: row;
    flex: 1;
    margin-left: 5%;
`;

const styles = StyleSheet.create({
    containerScroll: {
        width: wp('100%'),
        height: hp('56.5%')
    }
});

export default ShowMaterials;