import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import ShowMaterials from './ShowMaterials';
import ShowIssues from './ShowIssues';
import { BottomModal, ModalContent } from 'react-native-modals';
import normalize from 'react-native-normalize';
import axios from 'axios';
import { URL_BACK_END } from '../../environment/Environment'

class TabCategory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedTab: 0,
            modalCreateIssue: false,
            room: {}
        }
    }

    componentDidMount() {
        this.getRoomInfos();
    }

    getRoomInfos(){
        axios.get(`${URL_BACK_END}/api/rooms/informations/${this.props.url}`)
        .then(res => {
            if (res.data.roomName)
                this.setState({ room: res.data });
            else
                this.props.navigation.goBack()
        })
        .catch(error => console.log(error))
    }

    handleClickTab = (idTab) => {
        this.setState({ selectedTab: idTab });
    }

    render() {
        const { selectedTab, modalCreateIssue, room } = this.state;

        return (
            <View>
                <View style={styles.tabSectionStack}>
                    <View style={styles.tabSection}>
                        <TouchableOpacity
                            onPress={() => this.handleClickTab(0)}
                            style={
                                selectedTab === 0 ?
                                    (styles.selectedTab) :
                                    (styles.tab)
                            }
                        >
                            <Text style={styles.text}>Matériels</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.handleClickTab(1)}
                            style={
                                selectedTab === 1 ?
                                    (styles.selectedTab) :
                                    (styles.tab)
                            }
                        >
                            <Text style={styles.text}>Tickets</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.header}>
                        <View style={styles.iconRow}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Icon name="ios-arrow-back" style={styles.icon} />
                            </TouchableOpacity>
                                <Text style={styles.salle1}>{room.roomName}</Text>
                        </View>
                    </View>
                </View>
                {
                    selectedTab === 0 ? (
                        <ShowMaterials name_url={this.props.url}/>
                    ) : (
                            <ShowIssues name_url={this.props.url}/>
                        )
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tabSectionStack: {
        height: normalize('170', 'height'),
    },
    tabSection: {
        top: normalize('20', 'height'),
        left: 0,
        height: normalize('150', 'height'),
        position: "absolute",
        flexDirection: "row",
        right: 0,
        justifyContent: "space-around",
        borderColor: "rgba(128,128,128,1)",
        borderWidth: 0,
        borderBottomWidth: 1
    },
    tab: {
        width: 127,
        height: normalize('80', 'height'),
        alignSelf: "center",
        marginRight: 0,
        marginLeft: 0,
        justifyContent: "center"
    },
    selectedTab: {
        width: 127,
        height: 59,
        alignSelf: "center",
        marginRight: 0,
        marginLeft: 0,
        borderColor: "rgba(255,200,83,1)",
        borderWidth: 0,
        borderBottomWidth: 3,
        justifyContent: "center"
    },
    text: {
        color: "rgba(26,32,44,1)",
        fontSize: 14,
        alignSelf: "center"
    },
    header: {
        top: 0,
        left: 0,
        height: normalize('50', 'height'),
        position: "absolute",
        right: 0,
        flexDirection: "row"
    },
    icon: {
        color: "#1a202c",
        fontSize: 24,
        width: 18,
        height: 26
    },
    iconRow: {
        height: 26,
        flexDirection: "row",
        flex: 1,
        marginRight: 242,
        marginLeft: 23,
        marginTop: 17
    },
    salle1: {
        color: "#1a202c",
        fontWeight: "bold",
        fontSize: 20,
        marginLeft: 31,
        marginTop: 1
    }
});

export default TabCategory;