import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  TouchableOpacity,
  Text
} from "react-native";
import Floors from "../rooms/TabFloors";
import Footer from "../../components/include/Footer";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

function Home(props) {
  return (
    <View style={styles.container}>
      <StatusBar show />
      <View style={styles.headerTabs1Column}>
        <Floors {...props} navigation={props.navigation}/>
      </View>
      <View style={styles.headerTabs1ColumnFiller}></View>
      <Footer navigation={props.navigation}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)"
  },
  headerTabs1Column: {
    marginTop: hp('2%'),
    marginLeft: -2,
    marginRight: 2
  },
  headerTabs1ColumnFiller: {
    flex: 1
  }
});

export default Home;