import { StatusBar } from 'expo-status-bar'
import React, { Component } from 'react'
import { Button, StyleSheet, Text, View, Modal, Alert } from 'react-native'
import {Picker} from '@react-native-picker/picker'
import * as Location from 'expo-location'
import * as Permissions from 'expo-permissions'
import axios from 'axios'
import { URL_BACK_END } from '../environment/Environment'

class RoomLocalisation extends Component {
  state = {
    hasPermission: null,
    location: null,
    modalVisible: false,
    rooms: [],
    selectedRoom: 0
  }

  componentDidMount() {
    this.getRooms();
    this.getLocationAsync();
  }

  getRooms() {
    axios.get(`${URL_BACK_END}/api/rooms`)
      .then(res => {
        this.setState({ rooms: res.data.data, selectedRoom: res.data.data[0].attributes.name_url });
      })
      .catch(error => console.log(error))
  }

  async getLocationAsync() {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        location: 'Permission de géolocalisation non accordé.',
      });
    } else {
      this.setState({ hasPermission: true });
    }
 
    let location = await Location.getCurrentPositionAsync({accuracy: Location.LocationAccuracy.Highest});
    this.setState({ location: location });
   }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  }

  handleSubmit = (name_url, geoloc) => {
    if (name_url != null && geoloc != null)
    {
      let x = geoloc.coords.latitude
      let y = geoloc.coords.longitude
      let z = geoloc.coords.altitude
      let room = {
        localisation_gps: `${x},${y},${z}`
      }

      axios.patch(`${URL_BACK_END}/api/rooms/${name_url}`, {room})
        .then(res=> {
          this.setState({modalVisible: false})
        })
        .catch(error => Alert.alert(error))
    }
  }

  render() {
    const {hasPermission, location, modalVisible, rooms, selectedRoom} = this.state
    var text;
    if (hasPermission == null) {
        text = 'Demande de permission de localisation'
    }
    else if (hasPermission) {
        text = 'Récupération de la position en cours ...'
        if (location) {
          text = `Latitude : ${location.coords.latitude}\nLongitude : ${location.coords.longitude}\nAltitude : ${location.coords.altitude}`
        }
    }
    else {
        text = 'Permission de géolocalisation non accordé.'
    }

    return (
      <View style={styles.container}>

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {}}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text>À quelle salle voulez-vous assignez ces coordonnées ?</Text>
              <Picker
                selectedValue={selectedRoom}
                style={{ height: 50, width: 150 }}
                onValueChange={(itemValue, itemIndex) => this.setState({selectedRoom: itemValue})}
              >
                {
                  rooms.map(room =>
                      <Picker.Item key={room.id} label={room.attributes.name} value={room.attributes.name_url} />
                  )
                }
              </Picker>
              <Text></Text>
              <Button title='OK' onPress={() => this.handleSubmit(selectedRoom, location)}/>
              <Text></Text>
              <Button title='Annuler' onPress={() => this.setModalVisible(false)}/>
            </View>
          </View>
        </Modal>

        <Text>Vos coordonnées :</Text>
        <Text>{text}</Text>
        <Text></Text>
        <Button title='Assigner les coords à une salle' onPress={() => this.setModalVisible(true)}/>
        <StatusBar style="auto" />
      </View>
    );
  }
} export default RoomLocalisation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  }
});
