import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import EntypoIcon from "react-native-vector-icons/Entypo";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import normalize from 'react-native-normalize';

function Footer(props) {

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('QRCodeScanner')}
        style={styles.scanButton}
      >
        <MaterialCommunityIconsIcon
          name="barcode-scan"
          style={styles.icon3}
        ></MaterialCommunityIconsIcon>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('Home')}
        style={styles.mapButton}
      >
        <EntypoIcon name="home" style={styles.icon4}></EntypoIcon>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(45,55,72,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    flexWrap: "nowrap",
    height:  normalize(100, 'height')
  },
  scanButton: {
    width: 28,
    height: 28,
    justifyContent: "center"
  },
  icon3: {
    color: "rgba(255,255,255,1)",
    fontSize: 28
  },
  mapButton: {
    width: 33,
    height: 28
  },
  icon4: {
    color: "rgba(255,255,255,1)",
    fontSize: 28,
    marginTop: -2,
    marginLeft: 4
  }
});

export default Footer;