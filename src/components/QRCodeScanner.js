import * as React from 'react';
import { Text, View, StyleSheet, Button, Alert } from 'react-native';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Linking from 'expo-linking';

class QrCodeScanner extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
  };
  async componentDidMount() {
    this.getPermissionsAsync();
  }
  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }
  render() {
    const { hasCameraPermission, scanned } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Demande pour la permission d'accès à la caméra</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>Aucun acces à la caméra</Text>;
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center'
        }}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleQrCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />
        {scanned && (
          <Button 
            title="Relancer le scan"
            onPress={() => this.setState({ scanned: false })}
            style={styles.button}
          />
        )}
      </View>
    );
  }

  handleQrCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    this.props.navigation.navigate("InformationsRoom", { name_url: data })  
  };
}

const styles = StyleSheet.create({
    button: {
      backgroundColor: 'cornflowerblue',
    }
  });

export default QrCodeScanner;