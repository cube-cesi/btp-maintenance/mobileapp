import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import axios from 'axios';
import { URL_BACK_END } from '../environment/Environment';

class Home extends Component {
  state = {
    materials: []
  }

  componentDidMount() {
    axios.get(`${URL_BACK_END}/api/materials`)
      .then(res => {
        this.setState({ materials: res.data });
      })
  }

  render() {
    const {materials} = this.state;

    return (
      <View style={styles.container}>
        <Button title="Qr Code" onPress={() => this.props.navigation.navigate("QRCodeScanner")}/>
        <Button title="Géolocalisation" onPress={() => this.props.navigation.navigate("RoomLocalisation")}/>
        <Text>Les infos de tous les matériaux ci-dessous !</Text>
        {
          materials.map(material =>
           <View key={material.id} style={styles.materialItem}>
            <Text>  Matériel n°{material.id} </Text>
            <Text>  Nom : {material.name} </Text>
            <Text>  Quantité : {material.quantity} </Text>
           </View>
          )
        }
        <StatusBar style="auto" />
      </View>
    );
  }
} export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  materialItem: {
    flex: 0.12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
