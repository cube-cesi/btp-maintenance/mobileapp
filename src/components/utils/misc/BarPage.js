import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

class BarPage extends Component {
    constructor(props) {
        super(props);
    }

    nextPage = (nbPage) => {
        this.props.getRoomsByPage(nbPage + 1);
    }

    previousPage = () => {

    }

    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                <View style={styles.leftWrapper}>
                    {
                        this.props.nbPage > 1 &&
                        <TouchableOpacity style={styles.leftIconButton}>

                            <Icon name="ios-arrow-back" style={styles.leftIcon}></Icon>
                            <Text style={styles.retour}>Précédente</Text>
                        </TouchableOpacity>
                    }
                </View>

                <View style={styles.textWrapper}>
                    <Text numberOfLines={1} style={styles.page1}>
                        Page {this.props.nbPage}
                    </Text>
                </View>
                <View style={styles.rightWrapper}>
                    <TouchableOpacity onPress={() => this.nextPage(this.props.nbPage)} style={styles.rightIconButton}>
                        <Text style={styles.suivant}>Suivante</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        backgroundColor: "rgba(255,255,255,1)",
        paddingRight: 8,
        paddingLeft: 8
    },
    leftWrapper: {
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "center"
    },
    leftIconButton: {
        flexDirection: "row"
    },
    leftIcon: {
        color: "#007AFF",
        fontSize: 32
    },
    retour: {
        fontSize: 17,
        color: "#007AFF",
        paddingLeft: 5,
        alignSelf: "center"
    },
    textWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    page1: {
        fontSize: 17,
        lineHeight: 17,
        color: "#000"
    },
    rightWrapper: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center"
    },
    rightIconButton: {},
    suivant: {
        color: "#007AFF",
        fontSize: 17,
        alignSelf: "center"
    }
});

export default BarPage;