# Projet CESI

## Installation des composants nécessaires au déploiement

Pour déployer l'application sur les différents magasins d'applications vous aurez besoin de :
- Un compte [_Expo_](https://expo.io/) connecté à la dernière version [**d'expo-cli**](https://docs.expo.io/workflow/expo-cli/),
- Un compte développeur _Google_,
- Un compte développeur _Apple_ **payant**,
- La dernière version de [**Transporter**](https://apps.apple.com/us/app/transporter/id1450874784?mt=12),
- [Reconfigurer](https://docs.expo.io/distribution/building-standalone-apps/#2-configure-appjson) _app.json_ pour mettre à jour les informations si nécessaire.

## Construire les exécutables

Avant de déployer les applications sur _l'App Store_ et le _Google Play Store_, il faut d'abord compiler le projet pour _Android_ et _IOS_.

### Construire l'apk (Android)

Pour construire l'**apk**, exécutez cette commande à la racine du projet :
```sh
expo build:android
```
Pour le choix du type de build, sélectionnez **apk**.
![build_type_android](assets/doc/build_type_android.png) 
Si vous faites la construction du projet pour la 1ère fois, on va vous demander comment vous souhaitez gérer les certificats, laissez expo gérer le processus (1ère option). 

Vous pouvez maintenant voir les étapes de la construction et ensuite télécharger l’**apk** généré sur [_Expo_](https://expo.io/).

### Construire l'ipa (IOS)
Pour construire l'**ipa**, exécutez cette commande à la racine du projet :
```sh
expo build:ios
```
Pour le choix du type de build, sélectionnez **archive**.
![build_type_ios](assets/doc/build_type_ios.png)

Choisissez de vous connecter avec votre compte Apple développeur en appuyant sur _Y_ et entrez vos informations de connexion.
![credentials_apple](assets/doc/apple_credentials.png)

Pour toutes les demandes, laissez Expo gérer le processus (1ère option).

Vous pouvez maintenant voir les étapes de la construction et ensuite télécharger l’**ipa** généré sur [_Expo_](https://expo.io/).

## Déployer l'application

Après avoir généré vos exécutables, il faut maintenant les publier sur le _Google Play Store_ et _l'App Store_.

### Déployer sur le Google Play Store

Connectez-vous sur [_Google Play Console_](https://play.google.com/apps/publish), cliquez sur **Create Application**, puis remplissez et validez les formulaires.

![create_application_google](assets/doc/play_store/create_application_google.png)
![create_application_google2](assets/doc/play_store/create_application_google2.png)

Allez sur l'onglet **App releases** et cliquez sur **Manage Production**.

Cliquez sur **Create release** puis remplissez et validez le formulaire dans lequel vous ajouterez l'**apk** que vous avez généré.

![create_release_google](assets/doc/play_store/create_release_google.png)

Allez sur l'onglet **Content rating** puis remplissez et validez le formulaire.

Allez sur l'onglet **Pricing & distribution**, remplissez et validez le formulaire, puis cliquez sur **Ready to Publish** → **Manage releases**.
![ready_to_publish](assets/doc/play_store/ready_to_publish.png)

Allez sur la page **Edit release**, cliquez sur **Review** puis terminez en cliquant sur **Start Rollout to Production**.

Félicitations, vous avez maintenant déployé l'application sur le _Play Store_ !

### Déployer sur l'App Store

Connectez-vous sur [_ITunes Connect_](https://itunesconnect.apple.com/) et allez sur la page **My Apps**.

Cliquez sur le **+** en haut à gauche, sélectionnez **New App** puis remplissez et validez le formulaire.

- Sélectionnez le _bundle Id_ correspondant à la valeur de **bundleIdentifier** dans _app.json_,
- _SKU_ est un nom unique sans espace.

![add_app](assets/doc/app_store/add_app.png)

Ensuite sélectionnez l'application dans le tableau puis remplissez et validez le formulaire.

Allez sur la page **Pricing and Availability** puis remplissez et validez le formulaire.

Rendez-vous sur la page **1.0 Prepare for Submission** et entrez les informations.

![1.0_prepare_submission](assets/doc/app_store/1.0_prepare_submission.png)

- Ignorez les parties _IMessage App_ et _Apple Watch_
- À la fin du formulaire, sélectionnez **Manually release this version**

⚠️Pour ajouter l'**ipa** généré précédemment à la partie _Build_ du formulaire, suivez ces étapes :

1. Connectez-vous à _Transporter_,
2. Cliquez sur **Add app** et sélectionnez le fichier **ipa**,
3. Une fois que le fichier est ajouté, cliquez sur **Done**.
4. Sur le formulaire, cliquez sur le **+** à côté de _Build_ et sélectionnez le build que vous venez de créer.

![add_build](assets/doc/app_store/add_build.png)

Une fois les informations entrées, cliquez sur **Save**, puis sur **Submit for Review**.

Remplissez le dernier formulaire et cliquez sur **Submit**.

![IDFA](assets/doc/app_store/IDFA.png)

Félicitations, vous avez maintenant déployé l'application sur _l'App Store_ !
